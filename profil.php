<?php
include'header.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="css/profil.css">
  <title></title>
</head>
<body>
  <div id="main">

    <h1 id="titre">Bienvenue sur votre profil <?php echo $_SESSION['currentUser'][3];?> !</h1>
    <div id="infos">
      <div class="info">
        <p>• Ton Nom : <?php echo $_SESSION['currentUser'][1];?></p>
      </div>
      <div class="info">
        <p>• Ton Prénom : <?php echo $_SESSION['currentUser'][2];?></p>
      </div>
      <div class="info">
        <p>• Ton Pseudo : <?php echo $_SESSION['currentUser'][3];?></p>
      </div>
      <div class="info">
        <p>• Ton Mail : <?php echo $_SESSION['currentUser'][4];?></p>
      </div>
      <div class="info">
        <p>• Ton statut : <?php if ($_SESSION['auteur']==false) {
                                echo'lecteur';
                              }else{
                                if($_SESSION['admin']==true) {
                                  echo'administrateur';
                                }else{
                                  echo'auteur';
                                }}?></p>
      </div>
      <?php
      if ($_SESSION['auteur']==true) {
        ?>
        <div class="info">
        <p>• Nombre d'article écrit : <?php
          $db = new PDO('mysql:host=localhost;dbname=tuteo','admin_tuteo','1234');
          $sql = $db->prepare("SELECT COUNT(*) FROM articles WHERE id_user = :idUser");
          $sql->bindParam(':idUser',$_SESSION['currentUser'][0]);
          $sql->execute();
          $nbArticle = $sql->fetch();
          echo $nbArticle[0];
      ?></p>
      </div>

      <?php
    }

      ?>
    </div>

  </div>
</body>
</html>
