<?php
  session_start();

  if (isset($_SESSION['currentUser'])) {
    $_SESSION['connect']=true;
  }else{
    $_SESSION['connect']=false;
  }
  if (!isset($_SESSION['auteur'])) {
    $_SESSION['auteur']=null;
  }
  if (!isset($_SESSION['admin'])) {
    $_SESSION['admin']=null;
  }
  if (!isset($_SESSION['errormail'])) {
    $_SESSION['errormail']=null;
  }
  if (!isset($_SESSION['articleValide'])) {
    $_SESSION['articleValide']=null;
  }

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="css/header.css">
  <link rel="stylesheet" href="css/font.css">
  <title>Tuteo</title>
</head>
<body>
  <div id="header">

      <div id="startNav">
        <a href="index.php"><img id="logoTuteo" style="height:15vh;"src="img/tuteoLogoResize.png" alt=""></a>
      </div>

      <div id="middleNav">
            <a href="actus.php" class="menu">ACTUS</a>
            <a href="bugs.php" class="menu">BUGS</a>
            <a href="spotlight.php" class="menu">SPOTLIGHT</a>
      </div>

      <div id="endNav">
        <img id="logoUser" src="img/userLogoResize.png" alt="">
      </div>

      <div id="PhoneMenu">
        <img id="logoPhone" src="img/menuHamburger.png" alt="">
      </div>

  </div>

    <div id="menuDeroulant">

        <?php
          if ($_SESSION['connect']==true) {
              echo'<a id="boutonUser" href="deconnexion.php">Déconnexion</a>
                  <a id="boutonUser" href="profil.php">Votre Profil</a>';
                if($_SESSION['auteur']==true){
                echo '<a id="boutonUser" href="ecrireArticle.php">Ecrire un article</a>';
                  if ($_SESSION['admin']==true){
                      echo '<a id="boutonUser" href="Admin.php">Adminstration du site</a>';
                    }
            }
        }else{
          echo '<a id="boutonUser" href="connexion.php">Connexion</a>';
        };
           ?>


    </div>
    <div id="menuHamburger">
        <a id="navPhone"  href="actus.php">ACTUS</a>
        <a id="navPhone"  href="bugs.php">BUGS</a>
        <a id="navPhone"  href="spotlight.php">SPOTLIGHT</a>
        <?php
        if ($_SESSION['connect']==true) {
              echo'<a id="boutonUser" href="">Déconnexion</a>';
                if($_SESSION['auteur']==true){
                echo '<a id="boutonUser" href="">Votre Profil</a>
                      <a id="boutonUser" href="">Ecrire un article</a>';
                  if ($_SESSION['admin']==true) {
                      echo '<a id="boutonUser" href="">Adminstration du site</a>';
                    }
            }
        }else{
          echo '<a id="boutonUser" href="connexion.php">Connexion</a>';
        };
         ?>

    </div>

  </body>
<script src="JS/header.js"></script>
</html>
