  <?php
include 'header.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="css/index.css">
  <link rel="stylesheet" href="css/font.css">
  <title>Tuteo</title>
</head>
<body>
  <div id="divTitle">
        <div id="Title"><h1>TUTEO</h1></div>
        <div id="imgTitle"><img src="img/imgAccueil1.png" alt=""></div>
  </div>
  <div >
    <h2 class="mediumTitle">"Révolutionnez votre facon de penser l'informatique avec nous."</h2>

  </div>
  <div id="firstContent">
      <select name="Filter" id="Filter" >
        <option value="">Trier par</option>
        <option value="date">Date de parution</option>
        <option value="categoire">Catégorie</option>
        <option value="nbLike">Nombre de like</option>
      </select>
  </div>
  <div id="cards">
    <?php
    include'cards.php';
    $cards = new cards('*');
    ?>

    </div>
    <div id="divBtn">
      <button id="btnVoirPlus">Voir Plus</button>
    </div>
    <div class="categorie">
        <div id="TitleAndText">
          <h2 class="categoTitle">ACTUS :</h2>
          <p class="categoTexte">Dans la rubrique actualités vous pourrez retrouver des articles parlant de sujets de veille informatique. Toutes l’actu de l’informatique ce trouve ici !</p>
        </div>
        <a href="actus.php"><img id="img1" style="width:100%"src="img/veilleInfo.png" alt=""></a>
    </div>
    <div class="categorie">
        <a href="bugs.php"><img id="img2" style="width:100%"src="img/bugContent2.png" alt=""></a>
        <div id="TitleAndText">
          <h2 class="categoTitle">BUGS :</h2>
          <p class="categoTexte">Dans la rubrique bugs vous pourrez retrouver des articles parlant de bugs résolu ou non. N’hésitez pas à mettre un commentaire pour résoudre les bugs !</p>
        </div>
    </div>
    <div class="categorie">
        <a href="spotlight"></a>
        <div id="TitleAndText">
          <h2 class="categoTitle">SPOTLIGHT :</h2>
          <p class="categoTexte">Dans la rubrique spotlight vous pourrez retrouver des articles parlant de nos coups de cœur. Nous dénichons pour nous !</p>
        </div>
        <a  href="spotlight.php"><img id="img3" style="width:100%" src="img/Spotlight.png" alt=""></a>
    </div>
    <?php
    include 'footer.php';
  ?>
</body>
<script src="JS/index.js"></script>
</html>
