// Récupération des trois images
let img1 = document.getElementById('img1');
let img2 = document.getElementById('img2');
let img3 = document.getElementById('img3');


// HOVER IMG 1
img1.addEventListener('mouseover',event=>{
  img1.src ="img/veilleInfoHover.png";
});

img1.addEventListener('mouseout',event=>{
  img1.src ="img/veilleInfo.png";
});

 // HOVER IMG 2
img2.addEventListener('mouseover',event=>{
  img2.src ="img/bugContent2Hover.png";
});

img2.addEventListener('mouseout',event=>{
  img2.src ="img/bugContent2.png";
});

// HOVER IMG 3
img3.addEventListener('mouseover',event=>{
  img3.src ="img/SpotlightHover.png";
});

img3.addEventListener('mouseout',event=>{
  img3.src ="img/Spotlight.png";
});
