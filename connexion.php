<?php
     include 'header.php';

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="css/connexion.css">
  <title>Connexion</title>
</head>
<body>

    <div id="form">
      <h1 id="title">Connexion :</h1>
      <form id="inputs" action="#" method="post">
        <div class="inputLabel">
            <label for="pseudo">Mail :</label>
            <input id="mail" name="mail" type="text" placeholder=" mail">
        </div>
        <div class="inputLabel">
            <label for="password">Mot de passe :</label>
            <input id="password" name="password" type="password" placeholder=" mot de passe">
        </div>
        <input id="submit" type="submit" value="Connectez-vous">
        <a id="pasInscris" href="inscription.php">Pas encore inscris ?</a>

      </form>
    </div>

    <?php

$db = new PDO('mysql:host=localhost;dbname=tuteo','admin_tuteo','1234');



if (isset($_POST['mail'])) {
  // récupération du formulaire
  $mail = $_POST['mail'];
  $password = $_POST['password'];
  // Récupération des adresses mail de la db
  $verifMail = $db->prepare("SELECT mail_user FROM utilisateurs WHERE mail_user = :mail");
  $verifMail->bindParam(':mail',$mail,);
  $verifMail->execute();
  $reponseMail = $verifMail->fetch();
  // Récupération du mdp correspondant au mail
  $verifMdp = $db->prepare("SELECT mdp_user FROM utilisateurs WHERE mail_user = :mail");
  $verifMdp->bindParam(':mail',$mail,);
  $verifMdp->execute();
  $reponseMdp = $verifMdp->fetch();

  if (!$reponseMail || $password != $reponseMdp[0]){
    echo'<p style="text-align:center; color:red;">Email ou mot de passe incorrect.</p>';
  }else{
    $db->query("SET CHARACTER SET utf8");
    $connectUser = $db->prepare("SELECT * FROM utilisateurs WHERE mail_user = :mail");
    $connectUser->bindParam(':mail',$mail,);
    $connectUser->execute();
    $user = $connectUser->fetch();
    $_SESSION['currentUser']=$user;

    $connectAuteur = $db->prepare("SELECT auteur_user FROM utilisateurs WHERE mail_user = :mail");
    $connectAuteur->bindParam(':mail',$mail);
    $connectAuteur->execute();
    $auteur = $connectAuteur->fetch();

    $connectAdmin = $db->prepare("SELECT admin_user FROM utilisateurs WHERE mail_user = :mail");
    $connectAdmin->bindParam(':mail',$mail);
    $connectAdmin->execute();
    $admin = $connectAdmin->fetch();
    if ($auteur[0]) {
      $_SESSION['auteur']=true;
    }
    if ($admin[0]){
      $_SESSION['admin']=true;
    }
    header('location:index.php');
  }
}
?>
</body>
</html>
