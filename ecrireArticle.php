<?php
include'header.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="css/ecrireArticle">
  <title>Ecrire un article</title>
</head>
<body>
  <div id="form">
      <h1 id="title">Ecrire votre article</h1>
      <form id="inputs" action="#" method="post">
        <div class="inputLabel">
            <label for="titre">Titre de l'article :&nbsp;&nbsp;</label>
            <input id="titre" name="titre" type="text" placeholder=" titre" required maxlength="100">
        </div>
        <div class="inputLabel">
            <label for="categorie" >Choisir une catégorie :&nbsp;&nbsp;</label>
            <select class="input" name="categorie" id="categorie" required>
            <option class="option" value="">Catégories</option>
            <option class="option" value="Actualités">Actualités</option>
            <option class="option" value="Bugs">Bugs</option>
            <option class="option" value="Spotlight">Spotlight</option></select>
        </div>
        <div class="inputLabel">
            <label for="contenu">Contenu de l'article :&nbsp;&nbsp;</label>
            <textarea class="input" id="contenu" name="contenu" type="text" placeholder=" contenu" required rows="4" maxlength="10000"></textarea>
        </div>
        <input id="submit" type="submit" value="Publiez l'article">
        <?php
        if ($_SESSION['articleValide']==true) {
          echo'<p style="text-align:center; font-size:150%; color:green;">Article Publié</p>';
          $_SESSION['articleValide']=false;
        }
      ?>
</body>
</html>
<?php

  $db = new PDO('mysql:host=localhost;dbname=tuteo','admin_tuteo','1234');
  if (isset($_POST['titre']) && isset($_POST['contenu']) && isset($_POST['categorie'])) {
      $titre = $_POST['titre'];
      $contenu = $_POST['contenu'];
      $categorie  =  $_POST['categorie'];
      date_default_timezone_set('Europe/Paris');
      $date = date("Y-m-d H:i:s");
      $id_user = $_SESSION['currentUser'][0];


        $data= [$titre,$contenu,$categorie,$date,$id_user];

        $sql = $db->prepare("INSERT INTO articles (titre_article,contenu_article,categorie_article,date_article,id_user) VALUES (?,?,?,?,?)");
        $sql->execute($data);
        $_SESSION['articleValide']=true;
        header('location:ecrireArticle.php');
      };



?>
