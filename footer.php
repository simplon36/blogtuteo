<?php?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="css/footer.css">
  <title>Tuteo</title>
</head>
<body>
  <div id="footer">

      <div id="reseauxSociaux">
          <img class="img" src="img/insta.png" alt="">
          <img class="img" src="img/facebook.png" alt="">
      </div>

      <div id="contact">
          <div id="imgContact">
              <img class="imgMailPhone" src="img/mail.png" alt="">
              <img class="imgMailPhone" src="img/phone.png" alt="">
          </div>
          <div id="textContact">
              <p class="textMailPhone">tuteo@gmail.com</p>
              <p class="textMailPhone">06.45.57.82.34</p>
          </div>
     </div>

      <div id="politiquesConfidentialite">
          <a id="textPolitique" href="">Mentions légales</a>
          <a id="textPolitique"href="">Politique de confidentialité</a>
          <div id="copyright"><img id="imgCopyright" src="img/copyright.png" alt=""> <p id="textPolitique">Copyright 2023 Blog From Scratch</p></div>
     </div>
  </div>
</body>
</html>
